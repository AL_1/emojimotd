package me.diamonddev.emojimotd;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.util.CachedServerIcon;

public class PingListener implements Listener {

	Logger log = Bukkit.getLogger();

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onServerPing(ServerListPingEvent e) {
		boolean debug = Plugin.gi().getConfig().getBoolean("Options.Debug", false);
		String motdFormat = Plugin.gi().getConfig().getString("MOTD.format");
		String[] motds = Plugin.gi().getConfig().getStringList("MOTD.messages").toArray(new String[0]);
		String iconName = "server-icon.png";

		// Pick MOTD
		int rndMotd = new Random().nextInt(motds.length);
		String motd = String.format(motdFormat, motds[rndMotd]);
		motd = replacePH(motd);

		// Icon
		try {
			List<File> allIconsList = new ArrayList<File>();
			File[] iconFolder = (new File(Plugin.gi().getDataFolder().getAbsolutePath() + "/icons")).listFiles();
			for (File file : iconFolder) {
				if (file.isFile()) {
					allIconsList.add(file);
				}
			}
			File[] allIcons = allIconsList.toArray(new File[0]);
			int rndIcon = new Random().nextInt(allIcons.length);
			CachedServerIcon icon = Bukkit.loadServerIcon(allIcons[rndIcon]);
			iconName = allIcons[rndIcon].getName();
			e.setServerIcon(icon);
			if (motd.toLowerCase().contains("%emoji:")) {
				if (debug)
					log.info("Contains Definate Emoji");
				int start = motd.indexOf("%emoji:");
				int end = motd.replaceFirst("%emoji:", "").indexOf("%", start);
				String fileName = motd.substring(start, end + 7).replaceFirst("%emoji:", "");
				if (debug)
					log.info(fileName);
				File file = new File(Plugin.gi().getDataFolder().getAbsolutePath() + "/icons/" + fileName);
				if (file.exists()) {
					if (debug)
						log.info(fileName + " exits!");
					motd = motd.replaceAll("%emoji:" + fileName + "%", "");
					icon = Bukkit.loadServerIcon(file);
					iconName = file.getName();
					e.setServerIcon(icon);
				} else {
					if (debug)
						log.warning(fileName + " does not exits!");
				}
			} else {
				if (debug)
					log.info("Does not Contain Definate Emoji");
			}
		} catch (Exception e1) {
			e.setServerIcon(Bukkit.getServerIcon());
		}

		// Set MOTD
		e.setMotd(ChatColor.translateAlternateColorCodes('&', motd));

		FileLoader.logPingEvent(e, iconName);
		if (Plugin.gi().getConfig().getBoolean("Options.CheckFilesAfterPing"))
			FileLoader.CheckFiles();
	}

	public String replacePH(String str) {
		String[] colors = { "&1", "&2", "&3", "&4", "&5", "&6", "&7", "&8", "&9", "&a", "&b", "&c", "&d", "&e", "&f" };
		if (str.contains("%randomColor%")) {
			int rndColor = new Random().nextInt(colors.length);
			str = str.replaceFirst("%randomColor%", colors[rndColor]);
		}
		if (containsPH(str))
			str = replacePH(str);
		// str = ChatColor.translateAlternateColorCodes('&', str);
		return str;
	}

	public boolean containsPH(String str) {
		if (str.contains("%randomColor%"))
			return true;
		return false;
	}

}
