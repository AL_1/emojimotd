package me.diamonddev.emojimotd;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.io.Exception;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Plugin extends JavaPlugin {

	Logger logger = getLogger();
	File dataFolder = getDataFolder();

	@Override
	public void onEnable() {
		checkConfig();

		PluginManager manager = getServer().getPluginManager();
		manager.registerEvents(new PingListener(), this);

		FileLoader.CheckFiles();

		if (!getConfig().getBoolean("Statistics.opt-out", false)) {
			try {
			submitStatistics();
			} catch(Expectation e) {
			}
		}

	}

	public static Plugin gi() {
		return (Plugin) Bukkit.getPluginManager().getPlugin("EmojiMOTD");

	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		String helpMsg = ChatColor.GOLD + "/EmojiMOTD <reload|debug|logping>";
		String statusFormat = ChatColor.GOLD + "%s set to %s";
		if (args.length == 0) {
			sender.sendMessage(helpMsg);
		} else if (args.length == 1) {
			if (!sender.hasPermission("EmojiMOTD.Admin")) {
				sender.sendMessage(ChatColor.RED + "You do not have permission to run this command!");
				return true;
			}
			if (args[0].equalsIgnoreCase("reload")) {
				reloadConfig();
				sender.sendMessage(ChatColor.GOLD + getDescription().getFullName() + " reloaded.");
			} else if (args[0].equalsIgnoreCase("debug")) {
				boolean status = getConfig().getBoolean("Options.Debug");
				getConfig().set("Options.Debug", !status);
				sender.sendMessage(String.format(statusFormat, "LogPings", String.valueOf(!status).toUpperCase()));
				saveConfig();
			} else if (args[0].equalsIgnoreCase("logping")) {
				boolean status = getConfig().getBoolean("Options.LogPings");
				getConfig().set("Options.LogPings", !status);
				sender.sendMessage(String.format(statusFormat, "LogPings", String.valueOf(!status).toUpperCase()));
				saveConfig();
			} else {
				sender.sendMessage(helpMsg);
			}
		} else {
			sender.sendMessage(helpMsg);
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> returnList = new ArrayList<String>();
		if (args.length == 1) {
			if (args[0].toLowerCase().startsWith("r")) {
				returnList.add("reload");
			} else if (args[0].toLowerCase().startsWith("d")) {
				returnList.add("Debug");
			} else if (args[0].toLowerCase().startsWith("l")) {
				returnList.add("LogPing");
			} else {
				returnList.add("Debug");
				returnList.add("LogPing");
				returnList.add("reload");
			}
		}
		return returnList;
	}

	public void loadConfig() {
		getConfig().getDefaults();
		saveDefaultConfig();
		reloadConfig();
	}

	public void checkConfig() {
		if (getConfig().getInt("Config") == 4) {
			loadConfig();
		} else {
			createNewConfig();
		}
	}

	public void createNewConfig() {
		File oldFile = new File(getDataFolder().getAbsolutePath() + "/config.yml");
		File renamedFile;
		renamedFile = new File(
				getDataFolder().getAbsolutePath() + "/config_old." + System.currentTimeMillis() + ".yml");
		oldFile.renameTo(renamedFile);
		loadConfig();
	}

	public void submitStatistics() {
		String key = "YwoPkwrj2JBQJMZdBMuh";
		String plugin = getDescription().getName();
		String time = String.valueOf(System.currentTimeMillis());
		String ip = "UNKNOWN";
		String port = String.valueOf(getServer().getPort());
		String players = getServer().getOnlinePlayers().size() + "/" + getServer().getMaxPlayers();
		try {
			URL whatismyip = new URL("http://checkip.amazonaws.com");
			BufferedReader in = new BufferedReader(new InputStreamReader(whatismyip.openStream()));

			ip = in.readLine();
		} catch (Exception e) {
		}

		String url = "http://voltiac.ml/services/JavaPlugin/Statistics/collect.php";
		String prams = "key=" + key + "&plugin=" + plugin + "&time=" + time + "&ip=" + ip + "&port=" + port
				+ "&players=" + players + "";

		executePost(url + "?" + prams, "");

	}

	public static String executePost(String targetURL, String urlParameters) {
		HttpURLConnection connection = null;

		try {
			// Create connection
			URL url = new URL(targetURL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

			connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");

			connection.setUseCaches(false);
			connection.setDoOutput(true);

			// Send request
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.close();

			// Get Response
			InputStream is = connection.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			StringBuilder response = new StringBuilder(); // or StringBuffer if
															// Java version 5+
			String line;
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}
}
